FROM ubuntu

RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get upgrade -y
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y openssh-server vim sudo cron iproute2 traceroute git

RUN mkdir /var/run/sshd

RUN useradd -s /bin/bash -m worker
RUN echo "worker ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN echo 'worker:worker' | chpasswd

EXPOSE 22

USER worker

CMD ["/usr/bin/sudo", "/usr/sbin/sshd", "-D"]
