# educational docker environments

## requirements
docker: [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/)  
docker-compose: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)  

## run
`docker-compose up -d --scale linux=20`  
To start 20 instances of this educational linux environment.

## login
You can find the instance ssh ports by:  
`docker-compose ps`  

Just login via ssh (worker:worker):  
`ssh worker@domain -p INSTANCE_PORT`
